package model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by avispa on 1/12/2017.
 */
public class GameServer implements Runnable {
    private final int CONNECTIONS_LIMIT = 20;
    private int port = 5554;
    private ServerSocket serverSocket;
    private ExecutorService roomPool;

    public GameServer(final int PORT) {
        System.out.println("game server created");
        this.port = PORT;
        try {
            serverSocket = new ServerSocket(PORT);
            roomPool = Executors.newFixedThreadPool(CONNECTIONS_LIMIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    @Override
    public void run() {
        System.out.println("game server started");
        while (true) {
            try {
                System.out.println("server is running");
                Socket client1 = serverSocket.accept();
                System.out.println("client:" + client1.getInetAddress().toString() + " accepted");
                Socket client2 = serverSocket.accept();
                System.out.println("client:" + client2.getInetAddress().toString() + " accepted");
                roomPool.execute(new GameRoom(client1, client2));
                System.out.println("gameServer started room");
            } catch (Exception e) {
//                e.printStackTrace();
                break;
            }
        }
        System.out.println("game server stopped");
    }

    public class GameRoom implements Runnable {
        private Socket client1;
        private Socket client2;
        private DataOutputStream os1;
        private DataOutputStream os2;
        private DataInputStream is1;
        private DataInputStream is2;

        public GameRoom(Socket client1, Socket client2) {
            System.out.println("game room created");
            this.client1 = client1;
            this.client2 = client2;
            try {
                os1 = new DataOutputStream(client1.getOutputStream());
                os2 = new DataOutputStream(client2.getOutputStream());
                is1 = new DataInputStream(client1.getInputStream());
                is2 = new DataInputStream(client2.getInputStream());
                os1.writeChar('b');
                os2.writeChar('w');
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            System.out.println("game room started");
            try {
                String turn;
                while (true) {
                    turn = is1.readUTF();
                    System.out.println("game room received turn: " + turn + " from player 1");
                    if ("end".equals(turn)) break;
                    os2.writeUTF(turn);
                    if ("end".equals(turn)) break;
                    turn = is2.readUTF();
                    System.out.println("game room received turn: " + turn + " from player 2");
                    if ("end".equals(turn)) break;
                    os1.writeUTF(turn);
                    if ("end".equals(turn)) break;
                }
                is1.close();
                is2.close();
                os1.close();
                os2.close();
                client1.close();
                client2.close();
                System.out.println("room stopped");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
