package model;

/**
 * Created by avispa on 2/12/2017.
 */
public class Cell {
    CellState state;

    public Cell() {
        state = CellState.EMPTY;
    }

    public void setBlack() {
        if (state == CellState.PROB_BLACK || true) state = CellState.BLACK; //FIXME enable checking
//        else throw new RuntimeException("error while setting cell black");
    }

    public void setWhite() {
        if (state == CellState.PROB_WHITE || true) state = CellState.WHITE; //FIXME enable checking
//        else throw new RuntimeException("error while setting cell white");
    }

    public void setProbBlack() {
        if (state == CellState.EMPTY) state = CellState.PROB_BLACK;
        else throw new RuntimeException("error while setting cell prob black");
    }

    public void setProbWhite() {
        if (state == CellState.EMPTY) state = CellState.PROB_WHITE;
        else throw new RuntimeException("error while setting cell prob white");
    }

    public boolean isChip() {
        return state == CellState.BLACK || state == CellState.WHITE;
    }

    public boolean isEnemyChip(GameFSA fsa) {
        return this.isChip() && ((fsa.getState() == GameFSA.State.BLACK_TURN) == (this.state == CellState.WHITE));
    }

    public void flop() {
        switch (state) {
            case BLACK:
                state = CellState.WHITE;
                break;
            case WHITE:
                state = CellState.BLACK;
                break;
            case EMPTY:
            case PROB_BLACK:
            case PROB_WHITE:
                System.out.println("trying to flop non chip cell");
        }
    }

    public CellState getState() {
        return state;
    }

    public void setEmpty() { //// TODO: 2/12/2017 delete this stub
        state = CellState.EMPTY;
    }

    /**
     * Created by avispa on 2/12/2017.
     */
    public enum CellState {
        EMPTY, BLACK, PROB_BLACK, WHITE, PROB_WHITE
    }
}
