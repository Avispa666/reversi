package model;

import java.util.Objects;

/**
 * Created by avispa on 2/12/2017.
 */
public class Coord {
    public final int x;
    public final int y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Coord)) return false;
        Coord c = (Coord) obj;
        return this.x == c.x && this.y == c.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return x + "," + y;
     }

    public static Coord fromString(String enemyTurn) {
        String[] s = enemyTurn.split(",");
        return new Coord(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
    }
}
