package model;

/**
 * Created by avispa on 26/11/2017.
 */
public class GameFSA {
    private State state;

    public GameFSA() {
        state = State.BLACK_TURN;
    }

    public State getState() {
        return state;
    }

    public void nextTurn() {
        switch (state) {
            case WHITE_TURN:
                state = State.BLACK_TURN;
                break;
            case BLACK_TURN:
                state = State.WHITE_TURN;
                break;
            default:
                System.out.println("FSA: next turn invoked after win or draw");
        }
    }

    public void win(int white, int black) {
        switch (state) {
            case WHITE_TURN:
            case BLACK_TURN:
                if (white > black) state = State.WHITE_WIN;
                else state = State.BLACK_WIN;
                break;
            default:
                System.out.println("FSA: win invoked after win or draw");
        }
    }

    public void draw() {
        switch (state) {
            case BLACK_TURN:
            case WHITE_TURN:
                state = State.DRAW;
                break;
            default:
                System.out.println("FSA: draw invoked after win or draw");
        }
    }

    public enum State {
        BLACK_TURN, WHITE_TURN, BLACK_WIN, WHITE_WIN, DRAW
    }
}
