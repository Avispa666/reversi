package model;

import java.util.*;

/**
 * Created by avispa on 26/11/2017.
 */
public class GameField {
    private final int FIELD_SIZE = 8;
    private int blackCells;
    private int blackPossible;
    private int whiteCells;
    private int whitePossible;
    private Cell[][] field;
    private Map<Coord, Cell> changed;
    private GameFSA gameFSA;

    public GameField(GameFSA fsa) {
        field = new Cell[FIELD_SIZE][FIELD_SIZE];
        gameFSA = fsa;
        for (int i = 0; i < FIELD_SIZE; ++i) {
            for (int j = 0; j < FIELD_SIZE; ++j) {
                field[i][j] = new Cell();
            }
        }
        field[3][3].state = Cell.CellState.WHITE;
        field[4][4].state = Cell.CellState.WHITE;
        field[3][4].state = Cell.CellState.BLACK;
        field[4][3].state = Cell.CellState.BLACK;

        field[2][3].state = Cell.CellState.PROB_BLACK;
        field[3][2].state = Cell.CellState.PROB_BLACK;
        field[4][5].state = Cell.CellState.PROB_BLACK;
        field[5][4].state = Cell.CellState.PROB_BLACK;
        field[4][2].state = Cell.CellState.PROB_WHITE;
        field[2][4].state = Cell.CellState.PROB_WHITE;
        field[5][3].state = Cell.CellState.PROB_WHITE;
        field[3][5].state = Cell.CellState.PROB_WHITE;

        blackCells = whiteCells = whitePossible = blackPossible = 2;
//        whiteCells = 1;//// FIXME: 3/12/2017 only for testing
        changed = new TreeMap<>((o1, o2) -> {
            if (o1.equals(o2)) return 0;
            if ((o1.y < o2.y) || (o1.y == o2.y && o1.x < o2.x)) return -1;
            return 1;
        });
    }

    public Map<Coord, Cell> makeTurn(Coord c) {
        if (!checkBounds(c.x, c.y)) return null;
        if (!changed.isEmpty()) changed.clear();
        if (field[c.x][c.y].isChip()) return null;
        switch (gameFSA.getState()) {
            case BLACK_TURN:
//                if (field[x][y].state != Cell.CellState.PROB_BLACK) return null; //TODO enable after testing
                field[c.x][c.y].setBlack();
                ++blackCells;
                break;
            case WHITE_TURN:
//                if (field[x][y].state != Cell.CellState.PROB_WHITE) return null; //TODO enable after testing
                field[c.x][c.y].setWhite();
                ++whiteCells;
                break;
            case BLACK_WIN:
            case WHITE_WIN:
                System.out.println("cannot make turn after win");
                return null;
            case DRAW:
                System.out.println("cannot make turn after draw");
                return null;
        }
        flopChips(c);
        if (changed.isEmpty()) {
            if(field[c.x][c.y].getState() == Cell.CellState.WHITE) {
                --whiteCells;
            }
            else {
                --blackCells;
            }
            field[c.x][c.y].setEmpty();
            return null;
        }
        changed.put(c, field[c.x][c.y]);
        if (whiteCells + blackCells == FIELD_SIZE*FIELD_SIZE || whiteCells == 0 || blackCells == 0) {
            if (whiteCells == blackCells) gameFSA.draw();
            else gameFSA.win(whiteCells, blackCells);
        } else {
            switch (gameFSA.getState()) {
                case BLACK_TURN:
                    if (blackPossible != 0) gameFSA.nextTurn();
                    break;
                case WHITE_TURN:
                    if (whitePossible != 0) gameFSA.nextTurn();
                    break;
            }
        }
        return changed;
    }

    private void flopChips(Coord c) {
        int tx = c.x, ty = c.y;
        Queue<Coord> chipsToFlop = new LinkedList<>();
        while (checkBounds(tx-1,c.y) && field[tx-1][c.y].isEnemyChip(gameFSA)) { //left
            chipsToFlop.add(new Coord(--tx,c.y));
        }
        if (checkBounds(tx-1,c.y) && field[tx-1][c.y].isChip() && !field[tx-1][c.y].isEnemyChip(gameFSA)) {
            Coord co;
            while ((co = chipsToFlop.poll()) != null) flopChip(co);
        }
        if (!chipsToFlop.isEmpty()) chipsToFlop.clear();
        tx = c.x;
        while (checkBounds(tx+1,c.y) && field[tx+1][c.y].isEnemyChip(gameFSA)) { //right
            chipsToFlop.add(new Coord(++tx,c.y));
        }
        if (checkBounds(tx+1,c.y) && field[tx+1][c.y].isChip() && !field[tx+1][c.y].isEnemyChip(gameFSA)) {
            Coord co;
            while ((co = chipsToFlop.poll()) != null) flopChip(co);
        }
        if (!chipsToFlop.isEmpty()) chipsToFlop.clear();
        tx = c.x;
        while (checkBounds(c.x,ty-1) && field[c.x][ty-1].isEnemyChip(gameFSA)) { // up
            chipsToFlop.add(new Coord(c.x, --ty));
        }
        if (checkBounds(c.x,ty-1) && field[c.x][ty-1].isChip() && !field[c.x][ty-1].isEnemyChip(gameFSA)) {
            Coord co;
            while ((co = chipsToFlop.poll()) != null) flopChip(co);
        }
        if (!chipsToFlop.isEmpty()) chipsToFlop.clear();
        ty = c.y;
        while (checkBounds(c.x,ty+1) && field[c.x][ty+1].isEnemyChip(gameFSA)) { //down
            chipsToFlop.add(new Coord(c.x, ++ty));
        }
        if (checkBounds(c.x, ty+1) && field[c.x][ty+1].isChip() && !field[c.x][ty+1].isEnemyChip(gameFSA)) {
            Coord co;
            while ((co = chipsToFlop.poll()) != null) flopChip(co);
        }
        if (!chipsToFlop.isEmpty()) chipsToFlop.clear();
        ty = c.y;
        while (checkBounds(tx-1,ty-1) && field[tx-1][ty-1].isEnemyChip(gameFSA)) { //left up
            chipsToFlop.add(new Coord(--tx,--ty));
        }
        if (checkBounds(tx-1,ty-1) && field[tx-1][ty-1].isChip() && !field[tx-1][ty-1].isEnemyChip(gameFSA)) {
            Coord co;
            while ((co = chipsToFlop.poll()) != null) flopChip(co);
        }
        if (!chipsToFlop.isEmpty()) chipsToFlop.clear();
        ty = c.y;
        tx = c.x;
        while (checkBounds(tx-1,ty+1) && field[tx-1][ty+1].isEnemyChip(gameFSA)) { //left down
            chipsToFlop.add(new Coord(--tx,++ty));
        }
        if (checkBounds(tx-1,ty+1) && field[tx-1][ty+1].isChip() && !field[tx-1][ty+1].isEnemyChip(gameFSA)) {
            Coord co;
            while ((co = chipsToFlop.poll()) != null) flopChip(co);
        }
        if (!chipsToFlop.isEmpty()) chipsToFlop.clear();
        ty = c.y;
        tx = c.x;
        while (checkBounds(tx+1,ty-1) && field[tx+1][ty-1].isEnemyChip(gameFSA)) { //right up
            chipsToFlop.add(new Coord(++tx,--ty));
        }
        if (checkBounds(tx+1,ty-1) && field[tx+1][ty-1].isChip() && !field[tx+1][ty-1].isEnemyChip(gameFSA)) {
            Coord co;
            while ((co = chipsToFlop.poll()) != null) flopChip(co);
        }
        if (!chipsToFlop.isEmpty()) chipsToFlop.clear();
        ty = c.y;
        tx = c.x;
        while (checkBounds(tx+1,ty+1) && field[tx+1][ty+1].isEnemyChip(gameFSA)) { //right down
            chipsToFlop.add(new Coord(++tx,++ty));
        }
        if (checkBounds(tx+1,ty+1) && field[tx+1][ty+1].isChip() && !field[tx+1][ty+1].isEnemyChip(gameFSA)) {
            Coord co;
            while ((co = chipsToFlop.poll()) != null) flopChip(co);
        }
        if (!chipsToFlop.isEmpty()) chipsToFlop.clear();
        ty = c.y;
        tx = c.x;
    }

    private void flopChip(Coord c) {
        field[c.x][c.y].flop();
        switch (field[c.x][c.y].getState()) {
            case WHITE:
                ++whiteCells;
                --blackCells;
                break;
            case BLACK:
                ++blackCells;
                --whiteCells;
                break;
        }
        changed.put(c, field[c.x][c.y]);
    }

    private boolean checkBounds(int x, int y) {
        return x >= 0 && x < FIELD_SIZE && y >= 0 && y < FIELD_SIZE;
    }

    public Coord getScore() {
        return new Coord(whiteCells, blackCells);
    }
}
