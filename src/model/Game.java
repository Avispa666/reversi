package model;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import view.FieldView;
import view.GameView;

import java.util.Map;

/**
 * Created by avispa on 2/12/2017.
 */
public class Game {
    private GameView gameView;
    private GameFSA gameFsa;
    private GameField field;
    private Color color;

    public Game(GridPane fieldView, Label blackScore, Label whiteScore, Color color) {
        System.out.println("game started");
        gameView = new GameView(new FieldView(fieldView),blackScore, whiteScore);
        gameFsa = new GameFSA();
        field = new GameField(gameFsa);
        this.color = color;
    }

    public boolean clickCell(Coord c) {
        Map<Coord, Cell> map = field.makeTurn(c);
        if (map == null) return false;
        gameView.setScore(field.getScore());
        gameView.updateFieldView(map);
        if (!isYourTurn()) gameView.lock();
        else gameView.unlock();
        switch (gameFsa.getState()) {
            case BLACK_WIN:
            case WHITE_WIN:
            case DRAW:
                gameView.lock();
        }
        return true;
    }

    public void showAlert() {
        gameView.showAlert(gameFsa.getState(), color);
    }

    private boolean isYourTurn() {
        return (color == Color.WHITE) == (gameFsa.getState() == GameFSA.State.WHITE_TURN);
    }

    public boolean isEnd() {
        return gameFsa.getState() == GameFSA.State.BLACK_WIN || gameFsa.getState() == GameFSA.State.WHITE_WIN
                || gameFsa.getState() == GameFSA.State.DRAW;
    }

    public enum Color {
        BLACK, WHITE
    }
}
