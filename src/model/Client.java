package model;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by avispa on 3/12/2017.
 */
public class Client {
    private boolean isConnected;
    private ExecutorService serverNotifier;
    private GridPane fieldView;
    private Label blackScore;
    private Label whiteScore;
    private Game game;
    private Game.Color color;
    private Socket clientSocket;
    private DataInputStream is;
    private DataOutputStream os;

    public Client() {
        isConnected = false;
    }

    public void connect(GridPane fieldView, Label blackScore, Label whiteScore, InetAddress server, int port) {
        try {
            this.fieldView = fieldView;
            this.blackScore = blackScore;
            this.whiteScore = whiteScore;
            clientSocket = new Socket(server, port);
            is = new DataInputStream(clientSocket.getInputStream());
            os = new DataOutputStream(clientSocket.getOutputStream());
            serverNotifier = Executors.newSingleThreadExecutor();
            serverNotifier.execute(new Notifier(this, is, os));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clickCell(Coord coord) throws Exception{
        if (game.clickCell(coord)) {
            os.writeUTF(coord.toString());
            if (game.isEnd()) game.showAlert();
        }
    }

    private void  clickCellAsEnemy(Coord coord) {
        game.clickCell(coord);
        if (game.isEnd()) game.showAlert();
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void getNotification() throws Exception{
        if (!isConnected()) {
            System.out.println("receive notification with color");
            char c = is.readChar();
            if (c == 'b') {
                game = new Game(fieldView, blackScore, whiteScore, Game.Color.BLACK);
                fieldView.setDisable(false);
            }
            else game = new Game(fieldView, blackScore, whiteScore, Game.Color.WHITE);
            isConnected = true;
        } else {
            String enemyTurn = is.readUTF();
            System.out.println("enemy turn: " + enemyTurn);
            Coord coord = Coord.fromString(enemyTurn);
            Platform.runLater(() -> {
                clickCellAsEnemy(coord);
            });
        }
    }

    public void shutdownNow() {
//        try {
//            if (os != null) os.writeUTF("end");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        if (serverNotifier != null) serverNotifier.shutdownNow();
    }

    class Notifier implements Runnable{
        private DataInputStream is;
        private DataOutputStream os;
        private Client client;

        public Notifier(Client client, DataInputStream is, DataOutputStream os) {
            this.client = client;
            this.is = is;
            this.os = os;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (is.available() == 0) {
                        Thread.sleep(500);
                    } else {
                        System.out.println("notify client");
                        client.getNotification();
                    }
                } catch (Exception e) {
                    break;
                }
            }
            System.out.println("notifier stopped");
        }
    }
}
