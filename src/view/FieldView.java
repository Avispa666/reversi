package view;

import com.sun.javafx.geom.Vec2d;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import model.Cell;
import model.Coord;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Map;


/**
 * Created by avispa on 2/12/2017.
 */
public class FieldView {
    private GridPane fieldView;

    public FieldView(GridPane fieldView) {
        this.fieldView = fieldView;
    }

    public void changeCellsOnField(Map<Coord, Cell> map) {
        for (Map.Entry<Coord, Cell> e : map.entrySet()) {
            ImageView i = getImageView(e.getKey());
            String path = "";
            switch (e.getValue().getState()) {
                case BLACK:
                    path = getClass().getResource("/black.png").toString();
                    break;
                case PROB_BLACK:
                    break;
                case WHITE:
                    path = getClass().getResource("/white.png").toString();
                    break;
                case PROB_WHITE:
                    break;
            }
            i.setImage(new Image(path,64,64,true,true));
        }
        map.clear();
    }

    private ImageView getImageView(Coord c) {
        for (Node node : fieldView.getChildren()) {
            if (GridPane.getColumnIndex(node) == c.x && GridPane.getRowIndex(node) == c.y) {
                return (ImageView) node;
            }
        }
        return null;
    }

    public void lock() {
        fieldView.setDisable(true);
    }

    public void unlock() {
        fieldView.setDisable(false);
    }
}
