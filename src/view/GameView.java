package view;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import model.Cell;
import model.Coord;
import model.Game;
import model.GameFSA;

import java.util.Map;
import java.util.Optional;

/**
 * Created by avispa on 2/12/2017.
 */
public class GameView {
    private FieldView fieldView;
    private Label blackScore;
    private Label whiteScore;

    public GameView(FieldView fieldView, Label blackScore, Label whiteScore) {
        this.fieldView = fieldView;
        this.blackScore = blackScore;
        this.whiteScore = whiteScore;
    }

    public void setScore(Coord score) {
        whiteScore.setText(score.x + "");
        blackScore.setText(score.y + "");
    }

    public void showAlert(GameFSA.State event, Game.Color color) {
        String message = null;
        switch (event) {
            case BLACK_WIN:
                if (color == Game.Color.BLACK) message = "You win!";
                else message = "You lose!";
                break;
            case WHITE_WIN:
                if (color == Game.Color.WHITE) message = "You win!";
                else message = "You lose!";
                break;
            case DRAW:
                message = "Draw";
                break;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("The end");
        alert.setHeaderText(message);
        alert.setContentText("Play another game?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            // ... user chose OK
        } else {
            // ... user chose CANCEL or closed the dialog
        }
    }

    public void updateFieldView(Map<Coord, Cell> map) {
        fieldView.changeCellsOnField(map);
    }

    public void lock() {
        fieldView.lock();
    }

    public void unlock() {
        fieldView.unlock();
    }
}
