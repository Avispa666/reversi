package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    private Stage primaryStage;
    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("sample.fxml"));
//        Parent root = FXMLLoader.load();
        Parent root = loader.load();
        Controller c = loader.getController();
        c.setMain(this);
        this.primaryStage.setTitle("Reversi");
        this.primaryStage.setScene(new Scene(root,root.prefWidth(200) - 10, root.prefHeight(200)));
        this.primaryStage.setResizable(false);
        this.primaryStage.show();
        root.requestFocus();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
