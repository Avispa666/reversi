package controller;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Client;
import model.Coord;
import model.GameServer;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static javafx.scene.layout.GridPane.getColumnIndex;
import static javafx.scene.layout.GridPane.getRowIndex;

public class Controller {
    @FXML
    private Label whiteCount;
    @FXML
    private Label blackCount;
    @FXML
    private Label yourIp;
    @FXML
    private Button connectBtn;
    @FXML
    private TextField serverIp;
    @FXML
    private RadioButton ownServer;
    @FXML
    private RadioButton existedServer;
    @FXML
    private GridPane fieldView;

    private Main main;
    private Client client;
    private ExecutorService serverStarter;
    private ServerSocket serverSocket;
    private final int PORT = 5554;


    public Controller() throws Exception {
        client = new Client();
    }

    public void setMain(Main m) {
        this.main = m;
        main.getPrimaryStage().setOnCloseRequest(event -> {
            System.out.println("onclose request");
            if (serverStarter != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    //.....
                }
                serverStarter.shutdownNow();
            }
            if (client != null) client.shutdownNow();
            Platform.exit();
            System.exit(0);
        });
    }

    @FXML
    private void initialize() throws Exception {
        whiteCount.setText("2");
        blackCount.setText("2");
        serverIp.setText("127.0.0.1");
        ownServer.setSelected(true);
        fieldView.setDisable(true);

    }

    @FXML
    private void connect() {
        if (client.isConnected()) return;
        if (ownServer.isSelected()) {
            serverStarter = Executors.newSingleThreadExecutor();
            GameServer s = new GameServer(PORT);
            serverSocket = s.getServerSocket();
            serverStarter.execute(s);
            client.connect(fieldView, blackCount, whiteCount, Inet4Address.getLoopbackAddress(), PORT);
        } else {
            try {
                InetAddress server = InetAddress.getByName(serverIp.getText());
                client.connect(fieldView, blackCount, whiteCount, server, PORT);
            } catch (UnknownHostException e) {
                serverIp.setText("");
                return;
            }
        }
        connectBtn.setText("Connected");
    }

    @FXML
    private void fieldClick(Event e) throws Exception {
        Node node = (Node) e.getSource();
        int x = getColumnIndex(node);
        int y = getRowIndex(node);
        client.clickCell(new Coord(x, y));
    }
}
